import argparse
import configparser
import csv
import logging
import os
import sys
import shutil
import tempfile

from .generator import user_generator

def get_config():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config

def get_args():
    parser = argparse.ArgumentParser(description="Dump LDAP users into a key,value CSV file")
    parser.add_argument('csvfile', help="output csv file")
    parser.add_argument('key_attribute', default="cn")
    parser.add_argument('value_attribute', default="displayname")
    args = parser.parse_args()
    return args


def main():
    config = get_config()
    args = get_args()

    users = user_generator(config['ldap']['host'],
                           config['ldap']['bind_dn'],
                           config['ldap']['password'],
                           config['ldap']['search_base'],
                           args.key_attribute,
                           args.value_attribute)

    with tempfile.NamedTemporaryFile(mode="w", newline='', delete=False) as f:
        writer = csv.writer(f,quoting=csv.QUOTE_ALL,lineterminator=os.linesep)
        logging.info("Using %s",f.name)
        for user in users:
           writer.writerow( user )
        f.flush()
        size = f.tell()
        if os.path.exists(args.csvfile):
           oldsize = os.path.getsize(args.csvfile)
           if oldsize * 0.90 > size:
               logging.error("Not overwriting %s: existing file is much larger")
               sys.exit(1)
        shutil.move(f.name,args.csvfile)

if __name__ == '__main__':
   main()
