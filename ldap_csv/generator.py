from ldap3 import Server, Connection, SUBTREE

def user_generator(host,user, password, search_base, identifier, attribute, search_filter='(objectClass=user)'):
    server = Server(host = host, use_ssl=True)
    c = Connection(server, user=user, password=password,auto_bind=True)
    entry_generator = c.extend.standard.paged_search(search_base = search_base,
                                                     search_filter = '(objectClass=user)',
                                                     search_scope = SUBTREE,
                                                     attributes = [identifier, attribute],
                                                     paged_size = 1000,
                                                     generator=True)
    for entry in entry_generator:

        try:
           value = entry['attributes'][attribute]
        except IndexError:
           value = ""

        yield(entry['attributes'][identifier], value)
