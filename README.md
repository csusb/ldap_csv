In hindsight, I should have check to make sure this wheel hadn't already been invented.

Intended installation to a virtualenv

```
python3 -m venv /virtualenv/ldap-csv --prompt ldap-csv
source /virtualenv/ldap-csv/bin/activate
pip install .
```

Needs a config.ini in $PWD
```
[ldap]
#needs explict tls (e.g. 636/tcp, ldaps://)
host = domaincontroller.example.edu
bind_dn = roboticaccount@example.edu
password = some random bits here
search_base = cn=Users,dc=example,dc=edu
```

Usage:  dumps a key,value CSV suitable for a logstash translate filter
```
ldaptocsv displayname.csv cn displayname
```

An example user crontab
```
# m h  dom mon dow   command
08  03 *   *   *     cd /virtualenv/ldap_csv && bin/ldaptocsv displayname.csv cn displayname
```

An example partial logstash pipeline
```
filter {
   if [uid] {
       translate => {
          field => "[uid]"
          destination => "[displayname]"
          dictionary_path => "/virtualenv/ldap_csv/displayname.csv"
       }
   }
}
```
